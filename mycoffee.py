import argparse
from six.moves import input

ADDITIONAL_INGRIDIENTS = {"sugar": 0.2, "cream": 0.5, "cinnamon": 0.3, "top": 0.4, "no": 0}
BILL_FILE = "orders.txt"
BEVER_MENU = {"coffee": 1.5, "tea": 1, "latte": 3, "cappuccino": 2, "return": 0}
DELIMITER = "|"
NUM_CELLS = 3
ORDERS_COUNT_ROW_LEN = len("Number of sales")
SALESMAN_MENU = ["Start sale", "Quit"]
SELLER_NAME_ROW_LEN = len("Seller name")
SIDEBAR_COUNT = NUM_CELLS - 1


def get_menu(lst):
    for entry in lst:
        idx = lst.index(entry)
        print("{}. {}".format(idx + 1, entry))
    try:
        user_input = int(input("> "))

    except ValueError:
        print("Invalid input. Try again, please.")
    else:
        if user_input not in range(1, len(lst) + 1):
            print("Invalid input. Try again, please.")
            return
        return user_input


def get_menu_items(dict):
    lst = []
    for item in dict.keys():
        lst.append(item)
    return lst


def order_procesing(beverage):
    print("How many?")
    count = input("> ")
    if not str(count).isdigit():
        return 0, []
    count = int(count)
    if count <= 0:
        return 0, []
    beverage_price = count * BEVER_MENU[beverage]
    print(beverage_price)
    print("Would you like additional ingredients?")
    add_ingts_types = get_menu_items(ADDITIONAL_INGRIDIENTS)
    add_ingts = None
    extra_price = 0
    order_meta = [beverage, count]
    while add_ingts != "no":
        user_choice = get_menu(add_ingts_types)
        if not user_choice:
            continue
        add_ingts = add_ingts_types[user_choice - 1]
        if add_ingts == "no":
            continue
        order_meta.append(add_ingts)
        extra_price = extra_price + ADDITIONAL_INGRIDIENTS[add_ingts]
    return beverage_price + extra_price, order_meta


def save_order(filename, user_name, order_amount, order_meta):
    try:
        with open(filename, "a") as f:
            f.write(user_name + DELIMITER + str(order_amount) +
                    DELIMITER + " ".join(str(x) for x in order_meta) + "\n")
            print("Order saved")
    except Exception as e:
        print(e)


def collect_orders_statistic(filename):
    statistic = {}
    try:
        with open(filename) as f:
            for line in f:
                salesman_name = line.split("|")[0]
                salesman_revenue = float(line.split("|")[1])
                if salesman_name not in statistic.keys():
                    statistic[salesman_name] = {"orders_count": 1, "revenue": salesman_revenue}
                else:
                    statistic[salesman_name]["revenue"] += salesman_revenue
                    statistic[salesman_name]["orders_count"] += 1
            return statistic
    except Exception as e:
        print('Sorry, there are no statistic for you, yet')


def get_orders_statistic(statistic):

    # Define max str length in first row for statistic table
    max_user_name = max(len(key) for key in statistic.keys())
    if max_user_name < SELLER_NAME_ROW_LEN:
        max_user_name = SELLER_NAME_ROW_LEN

    # Print row names for statistic table
    print("Seller name" + " " * (max_user_name - SELLER_NAME_ROW_LEN) + DELIMITER + " " +
          "Number of sales" + DELIMITER + " " + "Total value ($)")

    # Print statistic table
    for key in statistic.keys():
        print(key + " " * (max_user_name - len(key)) +
              DELIMITER + " " +
              str(statistic[key]["orders_count"]) +
              " " * (ORDERS_COUNT_ROW_LEN - len(str(statistic[key]["orders_count"]))) +
              DELIMITER + " " +
              str(statistic[key]["revenue"]) +
              " " * (ORDERS_COUNT_ROW_LEN - len(str(statistic[key]["revenue"]))))

def main():

    is_authorized = False
    user_name = ""

    parser = argparse.ArgumentParser()
    parser.add_argument("manager", nargs='?',
                        default='salesman', help='To run the app in manager mode')
    args = parser.parse_args()

    # Check user role
    if args.manager.lower() != "manager":

        while True:

            if is_authorized:

                # Show interactive start app menu
                user_choice = get_menu(SALESMAN_MENU)

                if user_choice == SALESMAN_MENU.index("Quit") + 1:
                    print("Good bye!")
                    break

                # Show bever types menu
                elif user_choice == SALESMAN_MENU.index("Start sale") + 1:
                    bever_menu_items = get_menu_items(BEVER_MENU)
                    user_choice = get_menu(bever_menu_items)
                    if not user_choice:
                        continue
                    choosed_bever = bever_menu_items[user_choice - 1]

                    # Return to start menu
                    if choosed_bever == "return":
                        print("Okay! Maybe next time.")
                        continue

                    print("Ok, yuor order is: {}".format(choosed_bever))

                    # Start order processing
                    order_amount, order_meta = order_procesing(choosed_bever)
                    print(order_meta)
                    if not order_amount:
                        print("Can you repeat your order, please?")
                        continue
                    print("Order amount: {}".format(order_amount))

                    # Save order details to bill file
                    save_order(BILL_FILE, user_name, order_amount, order_meta)
            else:
                user_name = input("> Enter your name: ")
                is_authorized = True
    else:
        # Collect and show statiscic for all orders per Salesman
        orders_statistic = collect_orders_statistic(BILL_FILE)
        if orders_statistic:
            get_orders_statistic(orders_statistic)


if __name__ == '__main__':
    main()
